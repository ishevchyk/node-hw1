// requires...
const fs = require('fs');
const Joi = require('joi');
const path = require('path');
// const config = require('./passwords.json')

const pathToFiles = './files/';

const validateFile = (data) => {
  const schema = Joi.object({
    filename: Joi.string().required().regex(/.\btxt|.\bhtml|.\blog|.\bjson|.\byaml|.\bxml|.\bjs/)
      .messages({
        'string.pattern.base': 'Wrong file extension! (Only log, txt, json, yaml, xml, js file extensions allowed)',
        'any.required': 'Please specify filename parameter'
      }),
    content: Joi.string().min(1).required().messages({
      'string.empty': 'Empty content is not allowed! Please add some data.',
      'any.required': 'Please specify content parameter'
    })
  });
  return schema.validate(data);
}

function createFile (req, res, next) {
  const { error } = validateFile(req.body);
  if(error) return res.status(400).send({message: error.details[0].message});
  fs.writeFileSync(`${pathToFiles}${req.body.filename}`,req.body.content);
  // if(req.query.pass) {
  //   const obj = {
  //     [req.body.filename]: `${req.query.pass}`
  //   }
  //   config.passwords = {
  //     ...config.passwords,
  //     ...obj
  //   }
  //   fs.writeFileSync('passwords.json', `${JSON.stringify(config)}`)
  // }
  res.status(200).send({ "message": "File created successfully" });
}

function getFiles (req, res, next) {
  fs.readdir(pathToFiles,(error, files) => {
    if(error) return res.status(400).send(error.message);
    res.status(200).send({
      "message": "Success",
      "files": files
    });
  })

}

const getFile = (req, res, next) => {
  const files = fs.readdirSync('./files');
  const file = files.find(file => file === req.params.filename);
  if (!file) return res.status(400).send({
    "message": `No file with '${req.params.filename}' filename found`
  });
  const ext = path.extname(file).split('.')[1];
  const content = fs.readFileSync(`${pathToFiles}${file}`, 'utf8');
  const stats = fs.statSync(`${pathToFiles}${file}`);
  const data = stats.birthtime;

  res.status(200).send({
    "message": "Success",
    "filename": file,
    "content": content,
    "extension": ext,
    "uploadedDate": data
  });
}

const deleteFile = (req, res, next) => {
  const files = fs.readdirSync('./files');
  const file = files.find(file => file === req.params.filename);
  if (!file) return res.status(400).send({
    "message": `No file with '${req.params.filename}' filename found`
  });
  fs.unlink(`${pathToFiles}${file}`, (error) => {
    if (error) throw new Error();
    else res.status(200).send({
        "message": "Successfully removed.",
        "Removed file": file
      });
  })
}

const editFile = (req, res, next) => {
  const files = fs.readdirSync('./files');
  const file = files.find(file => file === req.params.filename);
  if (!file) return res.status(400).send({
    "message": `No file with '${req.params.filename}' filename found`
  });
  fs.writeFileSync(`${pathToFiles}${file}`, req.body.content, {flag: 'w'});
  res.status(200).send({ "message": `File ${file} was overwritten with new content!` });
};

module.exports = {
  createFile,
  getFiles,
  getFile,
  deleteFile,
  editFile
}
